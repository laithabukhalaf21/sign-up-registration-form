import { Component } from "react";
import logo from "./logo.png";
import "./App.css";
import FormControl from "@material-ui/core/FormControl";
import { InputLabel, Input, Button } from "@material-ui/core";

class App extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      surname: "",
      email: "",
      company: "",
      phoneNumber: 0,
      password: "",
      ConfirmPassword:""
    };
    
  }

  cleanState() {
    this.setState({ name: "" });
    this.setState({ surname: "" });
    this.setState({ email: "" });
    this.setState({ company: "" });
    this.setState({ phoneNumber: 0});
    this.setState({ password: "" });
    this.setState({ ConfirmPassword: "" });
  }

  handleChange = (e,newValue) => {
    const target = e.target;
    let value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
      // phoneNumber:(newValue)
    });
  };
  

  handleChange2 = (event, newValue) => {

    this.setState({ phoneNumber:newValue})
};

  consoleLog = () => {
  console.log(this.state)
  console.log(String(this.state.phoneNumber))
    this.cleanState();
  };

 


  render() {
    return (
      <div className="App">
        <img className="logo" src={logo} />

        <div className="FormCenter">
          <form className="FormFields"  >
            <div className="FormField">
              <FormControl>
                <InputLabel htmlFor="name">Name</InputLabel>
                <Input
                  id="name"
                  type="text"
                  className="FormField-input"
                  aria-describedby="my-helper-text"
                  placeholder="Name"
                  name="name"
                  value={this.state.name}
                  onChange={this.handleChange}
                  
                />
              </FormControl>
            </div>

            <div className="FormField">
              <FormControl>
                <InputLabel htmlFor="surname">Surname</InputLabel>
                <Input
                  id="surname"
                  type="text"
                  className="FormField-input"
                  aria-describedby="my-helper-text"
                  placeholder="Surname"
                  name="surname"
                  value={this.state.surname}
                  onChange={this.handleChange}
                />
              </FormControl>
            </div>

            <div className="FormField">
              <FormControl>
                <InputLabel htmlFor="company">Company</InputLabel>
                <Input
                  id="company"
                  type="text"
                  className="FormField-input"
                  aria-describedby="my-helper-text"
                  placeholder="Company"
                  name="company"
                  value={this.state.company}
                  onChange={this.handleChange}
                />
              </FormControl>
            </div>
            <div className="FormField">
              <FormControl>
                <InputLabel htmlFor="email">Email</InputLabel>
                <Input
                  id="email"
                  type="text"
                  className="FormField-input"
                  aria-describedby="my-helper-text"
                  placeholder="Email"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </FormControl>
            </div>
            <div className="FormField">
              <FormControl>
                <InputLabel htmlFor="PhoneNumber">PhoneNumber</InputLabel>
                <Input
                  id="PhoneNumber"
                  type="text"
                  className="FormField-input"
                  aria-describedby="my-helper-text"
                  placeholder="PhoneNumber"
                  name="PhoneNumber"
                  value={this.state.phoneNumber }
                  onChange={this.handleChange2}
                />
              </FormControl>
            </div>

            <div className="FormField">
              <FormControl>
                <InputLabel htmlFor="password">Password</InputLabel>
                <Input
                  id="password"
                  type="password"
                  className="FormField-input"
                  aria-describedby="my-helper-text"
                  placeholder="Password"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </FormControl>
            </div>
            <div className="FormField">
              <FormControl>
                <InputLabel htmlFor="confirm-password">
                  Confirm Password
                </InputLabel>
                <Input
                  id="confirm-password"
                  type="password"
                  className="FormField-input"
                  aria-describedby="my-helper-text"
                  placeholder="Confirm Password"
                  name="ConfirmPassword"
                  value={this.state.confirm}
                  onChange={this.handleChange}
                />
              </FormControl>
            </div>
            <Button
              onClick={this.consoleLog}
              variant="contained"
              color="primary"
              size="large"
            >
              Sign Up
            </Button>
          </form>
        </div>
      </div>
    );
  }
}

export default App;
